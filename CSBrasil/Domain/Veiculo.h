//
// Created by WIINC - Felipe on 04/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DomainBase.h"


/*
 * JSON VEICULO
 *
 * "id": 0,
    "plate": "QGG0588",
    "renavam": "1068728903",
    "brand": "GM",
    "model": "S-10 CD",
    "type": "SUV_PICKUP",
    "latestMaintenanceDate": 0,
    "status": "ACTIVE",
    "gtf": "PUBLIC_FLEETS",
    "acquisitionDate": 1445997600000,
    "year": "2015 / 2015",
    "customerCnpj": "08241788000130",
    "fullModel": "S10 CD LS 4X4 2.8 DIESEL",
    "mileage": 9800,
    "modelCode": 771
 *
 * */

@interface Veiculo : DomainBase


@property(nonatomic, strong) NSNumber *idVeiculo;
@property(nonatomic, strong) NSString *plate;
@property(nonatomic, strong) NSString *renavam;
@property(nonatomic, strong) NSString *brand;
@property(nonatomic, strong) NSString *model;
@property(nonatomic, strong) NSString *type;
@property(nonatomic, strong) NSString *latestMaintenanceDate;
@property(nonatomic, strong) NSString *status;
@property(nonatomic, strong) NSString *gtf;
@property(nonatomic, strong) NSString *acquisitionDate;
@property(nonatomic, strong) NSString *year;
@property(nonatomic, strong) NSString *customerCnpj;
@property(nonatomic, strong) NSString *fullModel;
@property(nonatomic, strong) NSNumber *mileage;
@property(nonatomic, strong) NSNumber *modelCode;

@end