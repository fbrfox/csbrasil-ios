//
// Created by WIINC - Felipe on 05/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DomainBase.h"

/*
 * JSON Loja
 *
 * "id": 176,
      "name": "SABACAR",
      "address": "RUA THULLER , 9 , JARDIM UNIVERSO (BRAS CUBAS)  MOGI DAS CRUZES - SP",
      "latitude": null,
      "longitude": null,
      "type": "WORKSHOP_MULTI_SERVICE",
      "brand": "MULTIMARCA",
      "status": "ACTIVE",
      "cnpj": "65631632000107",
      "phone": "11 34369173",
      "email": "sabacar@ig.com.br",
      "lastUpdate": "22/08/2016 14:28:44",
      "gtfList": [
        "LIGHT_PRIVATE_FLEETS",
        "HEAVY_PRIVATE_FLEETS",
        "PUBLIC_FLEETS",
        "ANOTHER_OPERATIONS_FLEETS"
      ]
 *
 * */

@interface Loja : DomainBase

@property(nonatomic, strong) NSNumber *id;
@property(nonatomic, strong) NSString *address;
@property(nonatomic, strong) NSNumber *latitude;
@property(nonatomic, strong) NSNumber *longitude;
@property(nonatomic, strong) NSString *type;
@property(nonatomic, strong) NSString *brand;
@property(nonatomic, strong) NSString *status;
@property(nonatomic, strong) NSString *cnpj;
@property(nonatomic, strong) NSString *phone;
@property(nonatomic, strong) NSString *email;
@property(nonatomic, strong) NSString *lastUpdate;
@property(nonatomic, strong) NSArray *gtfList;

@property(nonatomic, readwrite) BOOL noLocal;

@property(nonatomic, copy) NSString *name;
@end