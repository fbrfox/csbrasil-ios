//
// Created by WIINC - Felipe on 05/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Foto
@end

@interface Foto : JSONModel

@property(nonatomic, strong) NSString *photo;

- (instancetype)initWithPhoto:(NSString *)photo;

+ (instancetype)fotoWithPhoto:(NSString *)photo;


@end