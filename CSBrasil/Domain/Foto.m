//
// Created by WIINC - Felipe on 05/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import "Foto.h"


@implementation Foto {

}
- (instancetype)initWithPhoto:(NSString *)photo {
    self = [super init];
    if (self) {
        self.photo = photo;
    }

    return self;
}

+ (instancetype)fotoWithPhoto:(NSString *)photo {
    return [[self alloc] initWithPhoto:photo];
}

@end