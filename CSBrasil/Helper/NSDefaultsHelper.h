//
// Created by WIINC - Felipe on 04/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Veiculo;


@interface NSDefaultsHelper : NSObject


+ (void)salvarVeiculoAtual:(Veiculo *)veiculo;

+ (Veiculo *)obterVeiculoAtual;

+ (void)apagarVeiculoAtual;

+ (void)esquecerVeiculo;

+ (void)lembrarVeiculo:(BOOL)b;

+ (BOOL)veiculoLembrado;
@end