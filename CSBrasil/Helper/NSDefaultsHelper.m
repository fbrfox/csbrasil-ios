//
// Created by WIINC - Felipe on 04/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import "NSDefaultsHelper.h"
#import "Veiculo.h"


@implementation NSDefaultsHelper {

}

#define VEICULO_ATUAL_DEFAULT @"Veiculo"
#define LEMBRAR_VEICULO_ATUAL_DEFAULT @"Lembrar"


+(void)salvarVeiculoAtual:(Veiculo *)veiculo{

    NSString *jsonString = [veiculo toJSONString];
    [[NSUserDefaults standardUserDefaults] setObject:jsonString forKey:VEICULO_ATUAL_DEFAULT];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(Veiculo *)obterVeiculoAtual{

    NSString *jsonStringVeiculo = [[NSUserDefaults standardUserDefaults] stringForKey:VEICULO_ATUAL_DEFAULT];

    Veiculo *veiculo = [[Veiculo alloc] initWithString:jsonStringVeiculo error:nil];

    return veiculo;
}

+(void)apagarVeiculoAtual{

    [[NSUserDefaults standardUserDefaults] removeObjectForKey:VEICULO_ATUAL_DEFAULT];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void)esquecerVeiculo{

    [[NSUserDefaults standardUserDefaults] removeObjectForKey:LEMBRAR_VEICULO_ATUAL_DEFAULT];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)lembrarVeiculo:(BOOL)b {


    [[NSUserDefaults standardUserDefaults] setObject:@(b) forKey:LEMBRAR_VEICULO_ATUAL_DEFAULT];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


+ (BOOL)veiculoLembrado {

    return [[NSUserDefaults standardUserDefaults] boolForKey:LEMBRAR_VEICULO_ATUAL_DEFAULT];
}
@end