//
//  CALayer+UIColor.h
//  Movida
//
//  Created by Movida on 19/01/16.
//  Copyright © 2016 Movida. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface CALayer (UIColor)


// This assigns a CGColor to borderColor.
@property(nonatomic, assign) UIColor* borderUIColor;
@end
