//
//  CALayer+UIColor.m
//  Movida
//
//  Created by Movida on 19/01/16.
//  Copyright © 2016 Movida. All rights reserved.
//

#import "CALayer+UIColor.h"

@implementation CALayer (UIColor)

- (void)setBorderUIColor:(UIColor*)color {
    self.borderColor = color.CGColor;
}

- (UIColor*)borderUIColor {
    return [UIColor colorWithCGColor:self.borderColor];
}

@end
