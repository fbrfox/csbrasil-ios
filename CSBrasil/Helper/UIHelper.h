//
// Created by pedro henrique borges on 3/5/15.
// Copyright (c) 2015 Iterative. All rights reserved.
//

#import <Foundation/Foundation.h>




@interface UIHelper : NSObject



+ (UIHelper *)sharedInstance;

- (void)mostrarAlerta:(NSString *)mensagem;

- (void)mostrarAlertaErro:(NSString *)mensagem;

- (void)mostrarAlertaInfo:(NSString *)mensagem;

- (void)mostrarAlertaSucesso:(NSString *)mensagem delegate:(id)delegate;


- (void)mostrarAlerta:(NSString *)titulo mensagem:(NSString *)mensagem delegate:(id)delegate;

- (NSString *)formatarPreco:(NSNumber *)number;


- (void)showProgress;

- (void)showProgresswithText:(NSString *)text;

- (void)configuraNumberPadComDoneButton:(UITextField *)textField target:(id)target action:(SEL)action;


- (NSString *)randomStringWithLength:(int)len;


+ (NSString *)dataParaString:(NSDate *)date formato:(NSString *)formato;

- (void)hideProgress;

- (UIImage *)obterImagemPorCor:(UIColor *)color;

- (UIColor *)colorFromHexString:(NSString *)hexString;

- (UIColor *)colorFromHexString:(NSString *)hexString alpha:(CGFloat)alpha;

- (void)htmlInLabel:(UILabel *)label html:(NSString *)html;

+ (NSString *)adicionaDias:(int)dias data:(NSDate *)data formato:(NSString *)formato;

+ (CLLocationCoordinate2D)findCenterPoint:(CLLocationCoordinate2D)_lo1 :(CLLocationCoordinate2D)_loc2;

- (NSDate *)stringParaData:(NSString *)date formato:(NSString *)formato;

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

-(NSMutableString*) timeLeftSinceDate: (NSDate *) dateT;

- (NSString *)imageToNSString:(UIImage *)image;

- (NSString *)videoToNSString:(NSURL *)pathVideo;
@end