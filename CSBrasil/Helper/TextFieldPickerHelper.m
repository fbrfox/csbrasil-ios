//
// Created by pedro henrique borges on 3/6/15.
// Copyright (c) 2015 Iterative. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextFieldPickerHelper.h"


@implementation TextFieldPickerHelper


- (UIView *)inputView {


    _picker = [[UIPickerView alloc] init];
    [_picker setDelegate:self];
    [_picker setDataSource:self];
    [_picker setShowsSelectionIndicator:YES];

    return _picker;


}

- (UIView *)inputAccessoryView {
    
    self.row = 0;
    UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flexableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];

    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flexableItem, doneButton, nil]];

    return keyboardDoneButtonView;

}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return [self.delegate numberOfComponentsInPickerView:pickerView withTextField:self];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.delegate pickerView:pickerView numberOfRowsInComponent:component withTextField:self];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [self.delegate pickerView:pickerView titleForRow:row forComponent:component withTextField:self];
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {

    self.row = row;

}


- (IBAction)doneClicked:(id)sender {
    [self.delegate doneClicked:self withIndex:self.row];
}


@end