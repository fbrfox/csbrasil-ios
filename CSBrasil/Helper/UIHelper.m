//
// Created by pedro henrique borges on 3/5/15.
// Copyright (c) 2015 Iterative. All rights reserved.
//


#import <EasyImage/UIImage+Resize.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "AppDelegate.h"
#import "UIHelper.h"



@implementation UIHelper




+ (UIHelper *)sharedInstance {

    static UIHelper *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[UIHelper alloc] init];
    });

    return _sharedManager;
}


- (void)mostrarAlerta:(NSString *)mensagem {

    [self mostrarAlerta:@"" mensagem:mensagem];
}


- (void)mostrarAlertaErro:(NSString *)mensagem {

    [self mostrarAlerta:@"Ocorreu um erro!" mensagem:mensagem];

}

- (void)mostrarAlertaInfo:(NSString *)mensagem {

    [self mostrarAlerta:@"Atenção" mensagem:mensagem];
}

- (void)mostrarAlertaSucesso:(NSString *)mensagem delegate:(id)delegate {

    [self mostrarAlerta:@"Sucesso" mensagem:mensagem delegate:delegate];
}


- (void)mostrarAlerta:(NSString *)titulo mensagem:(NSString *)mensagem {

    [self mostrarAlerta:titulo mensagem:mensagem delegate:nil];
}

- (void)mostrarAlerta:(NSString *)titulo mensagem:(NSString *)mensagem delegate:(id)delegate {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:titulo message:mensagem delegate:delegate cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alertView show];
}





- (NSString *)formatarPreco:(NSNumber *)number {

    if([number doubleValue] == 0)
        return @"R$ 0,00";
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setDecimalSeparator:@","];
    [formatter setMinimumFractionDigits:2];

    NSString *amountString = [formatter stringFromNumber:number];

    return [NSString stringWithFormat:@"R$ %@", amountString];

}



-(void)showProgress{

    [self showProgresswithText:nil];

}

-(void)showProgresswithText:(NSString *)text {

    if(!text)
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    else
        [SVProgressHUD showWithStatus:text maskType:SVProgressHUDMaskTypeGradient];

}


-(void)hideProgress{

    [SVProgressHUD dismiss];

}


- (UIImage *)obterImagemPorCor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);

    UIImage *imageNav = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return imageNav;
}

- (UIColor *)colorFromHexString:(NSString *)hexString {

    return [self colorFromHexString:hexString alpha:1.0];
}

- (UIColor *)colorFromHexString:(NSString *)hexString alpha:(CGFloat)alpha {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:alpha];
}

-(void)htmlInLabel:(UILabel *)label html:(NSString *)html{


    NSError *err = nil;
    label.attributedText =
            [[NSAttributedString alloc]
                    initWithData: [html dataUsingEncoding:NSUTF8StringEncoding]
                         options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
              documentAttributes: nil
                           error: &err];

}

- (void)configuraNumberPadComDoneButton:(UITextField *)textField target:(id)target action:(SEL)action {

    UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Ok"
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:target
                                                                  action:action];

    doneButton.tag = textField.tag;

    UIBarButtonItem *flexableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];

    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flexableItem, doneButton, nil]];

    textField.inputAccessoryView = keyboardDoneButtonView;

}

NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

-(NSString *) randomStringWithLength: (int) len {

    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];

    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
    }

    return randomString;
}



+ (NSString *)dataParaString:(NSDate *)date formato:(NSString *)formato {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:formato];
    return [formatter stringFromDate:date];
}


+ (NSString *)adicionaDias:(int)dias data:(NSDate *)data formato:(NSString *)formato{
    

    NSDate *newDate1 = [data dateByAddingTimeInterval:60*60*24*dias];
    
    return    [self dataParaString:newDate1 formato:formato];
    
}


- (NSDate *)stringParaData:(NSString *)date formato:(NSString *)formato {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:formato];
    return [formatter dateFromString:date];
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(NSMutableString*) timeLeftSinceDate: (NSDate *) dateT {
    
    NSMutableString *timeLeft = [[NSMutableString alloc]init];
    
    NSDate *today10am =[NSDate date];
    
    NSInteger seconds = [today10am timeIntervalSinceDate:dateT];
    
    NSInteger days = (int) (floor(seconds / (3600 * 24)));
    if(days) seconds -= days * 3600 * 24;
    
    NSInteger hours = (int) (floor(seconds / 3600));
    if(hours) seconds -= hours * 3600;
    
    NSInteger minutes = (int) (floor(seconds / 60));
    if(minutes) seconds -= minutes * 60;
    
    if(days) {
        [timeLeft appendString:[NSString stringWithFormat:@"%ld dias", (long)days]];
    }
    
    if(hours) {
        [timeLeft appendString:[NSString stringWithFormat: @"%ld h", (long)hours]];
    }
    
    if(minutes) {
        [timeLeft appendString: [NSString stringWithFormat: @"%ld min",(long)minutes]];
    }
    
    if(seconds) {
        [timeLeft appendString:[NSString stringWithFormat: @"%ld seg", (long)seconds]];
    }
    
    return timeLeft;
}

-(NSString *)imageToNSString:(UIImage *)image
{
    UIImage *uiImage = [image scaleImageToSize:CGSizeMake( 320, 480)];

    NSData *imageData = UIImagePNGRepresentation(uiImage);
    return [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}



-(NSString *)videoToNSString:(NSURL *)pathVideo{


    NSData *data = [NSData dataWithContentsOfURL:pathVideo];

    NSString *base64String = [data base64EncodedStringWithOptions:0];

    return base64String;



}

+(CLLocationCoordinate2D)findCenterPoint:(CLLocationCoordinate2D)_lo1 :(CLLocationCoordinate2D)_loc2 {
    CLLocationCoordinate2D center;

    double lon1 = _lo1.longitude * M_PI / 180;
    double lon2 = _loc2.longitude * M_PI / 180;

    double lat1 = _lo1.latitude * M_PI / 180;
    double lat2 = _loc2.latitude * M_PI / 180;

    double dLon = lon2 - lon1;

    double x = cos(lat2) * cos(dLon);
    double y = cos(lat2) * sin(dLon);

    double lat3 = atan2( sin(lat1) + sin(lat2), sqrt((cos(lat1) + x) * (cos(lat1) + x) + y * y) );
    double lon3 = lon1 + atan2(y, cos(lat1) + x);

    center.latitude  = lat3 * 180 / M_PI;
    center.longitude = lon3 * 180 / M_PI;

    return center;
}



@end



