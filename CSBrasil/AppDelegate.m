//
//  AppDelegate.m
//  CSBrasil
//
//  Created by WIINC - Felipe on 08/09/16.
//  Copyright © 2016 CSBrasil. All rights reserved.
//

#import <JSONModel/JSONKeyMapper.h>
#import <JSONModel/JSONModel.h>
#import "AppDelegate.h"
#import "LocationTracker.h"
#import "APIClient.h"
#import "NSDefaultsHelper.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    [GMSServices provideAPIKey:@"AIzaSyDFyGC5NYjN29wIGYrFttLgaHIbaxwIam4"];
    [[LocationTracker sharedInstance]startLocationTracking];
    [JSONModel setGlobalKeyMapper:[[JSONKeyMapper alloc] initWithDictionary:@{@"success" : @"success",@"message" : @"message"}]];


    if([NSDefaultsHelper veiculoLembrado]){

        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        [self showHome:storyboard withTransiction:NO];
    }




    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



- (void)showPlacaRenavam:(UIStoryboard *)storyboard {
    UITabBarController *vc =  (UITabBarController*)[storyboard instantiateViewControllerWithIdentifier:@"homeTab"];
    [vc setSelectedIndex:1];

    if(!self.window)
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];


    [UIView transitionWithView:self.window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{ self.window.rootViewController = vc; }
                    completion:nil];

}

- (void)showHome:(UIStoryboard *)storyboard {

    [self showHome:storyboard withTransiction:YES];
}


- (void)showHome:(UIStoryboard *)storyboard withTransiction:(BOOL)transiction{

    UITabBarController *vc =  (UITabBarController*)[storyboard instantiateViewControllerWithIdentifier:@"homeTab"];
    [vc setSelectedIndex:1];

    if (transiction) {
        if(!self.window)
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];


        [UIView transitionWithView:self.window
                          duration:0.5
                           options:UIViewAnimationOptionTransitionFlipFromLeft
                        animations:^{ self.window.rootViewController = vc; }
                        completion:nil];
    }
    else{

        self.window.rootViewController = vc;
    }

}

-(void)showViewController:(UIViewController *)vc withTransition:(UIViewAnimationOptions)animation{


    [UIView transitionWithView:self.window
                      duration:0.5
                       options:animation
                    animations:^{ self.window.rootViewController = vc; }
                    completion:nil];

}

@end
