//
//  DetalheOficinaViewController.m
//  CSBrasil
//
//  Created by WIINC - Felipe on 10/10/16.
//  Copyright © 2016 CSBrasil. All rights reserved.
//

#import "DetalheOficinaViewController.h"
#import "Loja.h"
#import "MapsViewController.h"

@interface DetalheOficinaViewController ()
{
    
    IBOutlet UILabel *lbOficina;
    IBOutlet UILabel *lbCnpj;
    IBOutlet UILabel *lbEndereco;
    IBOutlet UILabel *lbTipo;
    IBOutlet UILabel *lbMarca;
    IBOutlet UILabel *lbTelefone;
    IBOutlet UISwitch *swEstouNaOficina;
    IBOutlet UILabel *lbEstou;
    IBOutlet UIButton *btSelecionar;
    IBOutlet UIImageView *btImageSelecionar;
}
@end

@implementation DetalheOficinaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    MapsViewController *maps = segue.destinationViewController;;

    maps.loja = self.lojaSelecionada;
    maps.showMaps = [segue.identifier isEqualToString:@"ComoChegarSegue"];

    [super prepareForSegue:segue sender:sender];



}


- (IBAction)onSelecionar:(id)sender {
}

- (IBAction)onComoChegarClick:(id)sender {
}


- (IBAction)onStreetViewClick:(id)sender {
}

@end
