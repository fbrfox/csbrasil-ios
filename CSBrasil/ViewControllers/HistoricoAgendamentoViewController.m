//
//  HistoricoAgendamentoViewController.m
//  CSBrasil
//
//  Created by WIINC - Felipe on 14/09/16.
//  Copyright © 2016 CSBrasil. All rights reserved.
//

#import "HistoricoAgendamentoViewController.h"
#import "HistoricoAgendamento.h"
#import "Loja.h"
#import "RegistroEntradaViewController.h"

@interface HistoricoAgendamentoViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    
    IBOutlet UITableView *tbHistorico;

}
@end

@implementation HistoricoAgendamentoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [super addLogoNavigation];
    [super addBackIcon];
    tbHistorico.tableFooterView = [UIView new];
    [tbHistorico reloadData];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return !_historicos || _historicos.count == 0 ? 1 : _historicos.count ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    if(!_historicos || _historicos.count == 0)
        return [tableView dequeueReusableCellWithIdentifier:@"cellEmpty" forIndexPath:indexPath];
    else
    {

        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellAgendamento" forIndexPath:indexPath];

        UILabel *lbNumero = [cell viewWithTag:1];
        UILabel *lbAgendamento = [cell viewWithTag:2];
        UILabel *lbStatus = [cell viewWithTag:3];
        UILabel *lbOficina = [cell viewWithTag:4];


        HistoricoAgendamento *historicoAgendamento = _historicos[(NSUInteger) indexPath.row];

        lbNumero.text = [historicoAgendamento.id stringValue];
        lbAgendamento.text = historicoAgendamento.scheduleDate;
        lbStatus.text = historicoAgendamento.status;
        lbOficina.text = historicoAgendamento.authorizedWorkshop.name;

        return cell;
    }


}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    //Registrar Entrada
    if(_source == 1){

        RegistroEntradaViewController *registro = [self.storyboard instantiateViewControllerWithIdentifier:@"RegistroEntradaViewController"];
        registro.historicoAgendamento = _historicos[(NSUInteger) indexPath.row];
        [self.navigationController pushViewController:registro animated:YES];

    }
    else if(_source == 2){

    }


}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/






@end
