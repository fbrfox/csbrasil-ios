//
//  RotaOficinaViewController.m
//  CSBrasil
//
//  Created by WIINC - Felipe on 14/09/16.
//  Copyright © 2016 CSBrasil. All rights reserved.
//



#import "RotaOficinaViewController.h"
#import "Loja.h"
#import "DetalheOficinaViewController.h"


@interface RotaOficinaViewController ()<GMSMapViewDelegate>
{
    IBOutlet GMSMapView *mapView;
    Loja *lojaSelecionada;
}
@end

@implementation RotaOficinaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [super addLogoNavigation];

    mapView.delegate = self;

    [self initLojaFake];
    [self addMinhaPosicaoMapa];
    [self addLojasMapa];

}

- (void)addMinhaPosicaoMapa {
    mapView.myLocationEnabled = YES;
    CLLocation *location = [[LocationTracker sharedInstance] myLastLocation];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude
                                                            longitude:location.coordinate.longitude zoom:14];
    [mapView animateToCameraPosition:camera];
}

- (void)addLojasMapa {
    GMSMarker *startMarker = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake([lojaSelecionada.latitude doubleValue], [lojaSelecionada.longitude doubleValue])];
//    startMarker.appearAnimation = kGMSMarkerAnimationPop;
    startMarker.title = lojaSelecionada.name;
    startMarker.map = mapView;
    startMarker.icon = [UIImage imageNamed:@"pin"];

    startMarker.userData = lojaSelecionada;
}

- (void)initLojaFake {
    lojaSelecionada = [[Loja alloc] init];
    lojaSelecionada.brand = @"MULTIMARCA";
    lojaSelecionada.name = @"SABACAR";
    lojaSelecionada.address = @"RUA THULLER , 9 , JARDIM UNIVERSO (BRAS CUBAS)  MOGI DAS CRUZES - SP";
    lojaSelecionada.latitude = @(-23.5034999);
    lojaSelecionada.longitude = @(-46.714765);
    lojaSelecionada.type = @"WORKSHOP_MULTI_SERVICE";
    lojaSelecionada.status = @"ACTIVE";
    lojaSelecionada.cnpj = @"65631632000107";
    lojaSelecionada.phone = @"11 34369173";
    lojaSelecionada.email = @"sabacar@ig.com.br";
}

- (void)mapView:(GMSMapView *)mapView1 didTapInfoWindowOfMarker:(GMSMarker *)marker {

    [self performSegueWithIdentifier:@"DetalheLojaSegue" sender:marker.userData];

}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.

    DetalheOficinaViewController *detalhe = segue.destinationViewController;
    detalhe.lojaSelecionada = sender;

}


@end
