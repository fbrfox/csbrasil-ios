//
//  RegistroEntradaViewController.h
//  CSBrasil
//
//  Created by WIINC - Felipe on 14/10/16.
//  Copyright © 2016 CSBrasil. All rights reserved.
//

#import "BaseViewController.h"

@class HistoricoAgendamento;


@interface RegistroEntradaViewController : BaseViewController

@property(nonatomic, strong) HistoricoAgendamento *historicoAgendamento;

@end
