//
//  RegistroEntradaViewController.m
//  CSBrasil
//
//  Created by WIINC - Felipe on 14/10/16.
//  Copyright © 2016 CSBrasil. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "RegistroEntradaViewController.h"
#import "UIHelper.h"
#import "APIClient.h"
#import "RegistrarEntradaRequest.h"
#import "RegistrarEntrada.h"
#import "HistoricoAgendamento.h"
#import "NSDefaultsHelper.h"
#import "Veiculo.h"
#import "Loja.h"

@interface RegistroEntradaViewController ()<UIAlertViewDelegate>
{
    
    IBOutlet UITextField *tfKmAtual;
    IBOutlet UITextField *tdDataAtual;
    IBOutlet UITextField *tfOficina;
    IBOutlet UITextView *tfObservacao;
}
@end

@implementation RegistroEntradaViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    Veiculo *veiculo = [NSDefaultsHelper obterVeiculoAtual];
    tfKmAtual.text = [veiculo.mileage stringValue];
    tdDataAtual.text = [UIHelper dataParaString:[NSDate date]formato:@"dd/MM/yyyy HH:mm"];
    tfOficina.text = _historicoAgendamento.authorizedWorkshop.name;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onEnviarClick:(id)sender {


    [[UIHelper sharedInstance] showProgress];

    RegistrarEntradaRequest *request = [self preencheRequest];

    [[APIClient sharedManager] registrarEntrada:request completion:^(ResponseBase *response, NSString *errorMessage) {

        [[UIHelper sharedInstance] hideProgress];

        if(!response.success){

            [[UIHelper sharedInstance] mostrarAlertaErro:response.message];
        }
        else
            [[UIHelper sharedInstance] mostrarAlertaSucesso:response.message delegate:self];

    }];
}

- (RegistrarEntradaRequest *)preencheRequest {
    RegistrarEntradaRequest *request = [[RegistrarEntradaRequest alloc] init];
    request.request = [[RegistrarEntrada alloc] init];
    request.request.entranceDate = [NSString stringWithFormat:@"%d",(int)[[NSDate date] timeIntervalSince1970]];
    request.request.scheduling = _historicoAgendamento;

    CLLocation *lastLocation = [LocationTracker sharedInstance].myLastLocation;
    if(lastLocation){
        request.request.latitude = @(lastLocation.coordinate.latitude);
        request.request.longitude = @(lastLocation.coordinate.longitude);
    }

    request.request.observation = tfObservacao.text;
    return request;
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {

    [self.navigationController popToRootViewControllerAnimated:YES];

}


@end
