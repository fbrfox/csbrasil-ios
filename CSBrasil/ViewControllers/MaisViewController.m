//
//  MaisViewController.m
//  CSBrasil
//
//  Created by WIINC - Felipe on 14/10/16.
//  Copyright © 2016 CSBrasil. All rights reserved.
//

#import "MaisViewController.h"
#import "HistoricoAgendamentoViewController.h"
#import "ListarAgendamentoRequest.h"
#import "NSDefaultsHelper.h"
#import "UIHelper.h"
#import "ListarAgendamentoResponse.h"
#import "ListarAgendamento.h"
#import "APIClient.h"
#import "Veiculo.h"
#import "PlacarRenavamViewController.h"
#import "RegistroEntradaViewController.h"
#import "HistoricoAgendamento.h"
#import "Loja.h"
#import "AgendarVistoriaRequest.h"
#import "Agendamento.h"
#import "VeiculoAgendarVistoria.h"
#import "LojaAgendarVistoria.h"
#import "UsuarioAgendarVistoria.h"


@interface MaisViewController () <UIAlertViewDelegate>
{
    HistoricoAgendamento *historicoSelecionado;
}
@end

@implementation MaisViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)onEntradaClick:(id)sender {

    [self buscarPorStatus:@[@"SCHEDULED"] source:1];

}


- (IBAction)onSaidaClick:(id)sender {

    [self buscarPorStatus:@[@"READY_UNDELIVERABLE"] source:2];

}


- (IBAction)onTrocarVeiculoClick:(id)sender {
    [NSDefaultsHelper apagarVeiculoAtual];
    [NSDefaultsHelper esquecerVeiculo];

    PlacarRenavamViewController *placarRenavamViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PlacarRenavamViewController"];
    [DELEGATE showViewController:placarRenavamViewController withTransition:UIViewAnimationOptionTransitionFlipFromRight];
}


- (IBAction)onSairClick:(id)sender {

    [NSDefaultsHelper apagarVeiculoAtual];
    [NSDefaultsHelper esquecerVeiculo];


    __kindof UIViewController *controller = [self.storyboard instantiateInitialViewController];
    [DELEGATE showViewController:controller withTransition:UIViewAnimationOptionTransitionFlipFromRight];

}


- (void)buscarPorStatus:(NSArray *)statusArray source:(int)source {


//

    ListarAgendamentoRequest *request = [self preencherRequest];

    [[UIHelper sharedInstance] showProgress];

    [[APIClient sharedManager] listarAgendamentos:request completion:^(ListarAgendamentoResponse *response, NSString *errorMessage) {

        [[UIHelper sharedInstance] hideProgress];

        if (!response.success) {

            [[UIHelper sharedInstance] mostrarAlertaErro:response.message];

        }
        else {

            NSMutableArray *arrayHistoricos = [[NSMutableArray alloc] init];

            for (NSString *status in statusArray) {
                NSArray *agendamentos = [response.agendamentos filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.status == %@", status]];
                [arrayHistoricos addObjectsFromArray:agendamentos];
            }


            if (arrayHistoricos.count > 1) {
                HistoricoAgendamentoViewController *historicoAgendamentoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HistoricoAgendamentoViewController"];
                historicoAgendamentoViewController.historicos = arrayHistoricos;
                historicoAgendamentoViewController.source = source;
                [self.navigationController pushViewController:historicoAgendamentoViewController animated:YES];
            }
            else if (source == 1) {
                if (arrayHistoricos.count == 1) {
                    RegistroEntradaViewController *registro = [self.storyboard instantiateViewControllerWithIdentifier:@"RegistroEntradaViewController"];
                    registro.historicoAgendamento = arrayHistoricos[0];
                    [self.navigationController pushViewController:registro animated:YES];
                }
                else
                    [[UIHelper sharedInstance] mostrarAlertaErro:@"Não é possível realizar a saída de um veiculo, pois não há nenhum agendamento com o Status esperado!"];

            }
            else if (source == 2) {

                if (arrayHistoricos.count == 1) {
                    [self registrarSaida:arrayHistoricos[0]];
                }
                else {
                    [[UIHelper sharedInstance] mostrarAlertaErro:@"Não é possível realizar a saída de um veiculo, pois não há nenhum agendamento com o Status esperado!"];
                }
            }
        }


    }];


}

- (ListarAgendamentoRequest *)preencherRequest {
    Veiculo *veiculo = [NSDefaultsHelper obterVeiculoAtual];

    ListarAgendamentoRequest *request = [[ListarAgendamentoRequest alloc] init];
    request.request = [[ListarAgendamento alloc] init];
    request.request.vehiclePlate = veiculo.plate;
    return request;
}


- (void)registrarSaida:(HistoricoAgendamento *)historico {

    NSString *msg = [NSString stringWithFormat:@"Deseja confirmar a saída do veiculo da manutenção realizada na Oficina %@", historico.authorizedWorkshop.name];

    UIAlertView *view = [[UIAlertView alloc] initWithTitle:@"Registro de Saída" message:msg delegate:self cancelButtonTitle:@"Não" otherButtonTitles:@"Sim", nil];

    [view show];


    historicoSelecionado = historico;

}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {

    if (buttonIndex == 1) {
        [self registrarSaidaApi];
    }
}

- (void)registrarSaidaApi {
    AgendarVistoriaRequest *request = [[AgendarVistoriaRequest alloc] init];
    request.request = [[Agendamento alloc] init];
    request.request.kilometer = historicoSelecionado.kilometer;
    request.request.photoList = historicoSelecionado.photoList;
    request.request.vehicle = [[VeiculoAgendarVistoria alloc] init];
    request.request.vehicle.plate = historicoSelecionado.vehicle.plate;
    request.request.vehicle.renavam = historicoSelecionado.vehicle.renavam;
    request.request.type = historicoSelecionado.type;
    request.request.alreadyInTheWorkshop = historicoSelecionado.alreadyInTheWorkshop;
    request.request.attribute = historicoSelecionado.attribute;
    request.request.conclusionDate = historicoSelecionado.conclusionDate;
    request.request.authorizedWorkshop = [[LojaAgendarVistoria alloc] init];
    request.request.authorizedWorkshop.id = historicoSelecionado.authorizedWorkshop.id;
    request.request.driverEmail = historicoSelecionado.driverEmail;
    request.request.driverName = historicoSelecionado.driverName;
    request.request.employeeName = historicoSelecionado.employeeName;
    request.request.entranceDate = historicoSelecionado.entranceDate;
    request.request.exitDate = historicoSelecionado.exitDate;
    request.request.id = historicoSelecionado.id;
    request.request.latitude = historicoSelecionado.latitude;
    request.request.longitude = historicoSelecionado.longitude;
    request.request.observation = historicoSelecionado.observation;
    request.request.phone = historicoSelecionado.phone;
    request.request.registrationDate = historicoSelecionado.registrationDate;
    request.request.scheduleDate = historicoSelecionado.scheduleDate;
    request.request.status = @"READY_UNDELIVERABLE";
    request.request.user  = [[UsuarioAgendarVistoria alloc] initWithCpf:historicoSelecionado.user.cpf
                                                                       nome:historicoSelecionado.user.nome];


    [[UIHelper sharedInstance] showProgress];

    [[APIClient sharedManager] registrarSaida:request completion:^(ResponseBase *response, NSString *errorMessage) {

            [[UIHelper sharedInstance] hideProgress];

            if(!response.success)
                [[UIHelper sharedInstance] mostrarAlertaErro:response.message];
            else{


            }


        }];
}


@end
