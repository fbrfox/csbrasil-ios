//
//  BaseGPSViewController.h
//  Movida
//
//  Created by Movida on 04/01/16.
//  Copyright © 2016 Movida. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CLLocation;
@import CoreLocation;

@interface BaseGPSViewController : BaseViewController

@property(nonatomic, strong) CLLocation *currentLocation;

@property(nonatomic, readwrite) BOOL naoDesativaUpdate;

- (BOOL)verificaStatusGPS;

-(void)obterCidadeUsuarioCompletionHandler:(void(^)(CLPlacemark *local,NSString *error))completionHandler;


-(void)obterCidadeDigitada:(NSString *)cidadeDigitada completionHandler:(void(^)(NSArray *locais,NSString *error))completionHandler;


-(void)startUpdate;
@end
