//
//  BaseGPSViewController.m
//  Movida
//
//  Created by Movida on 04/01/16.
//  Copyright © 2016 Movida. All rights reserved.
//

#import "BaseGPSViewController.h"
@import CoreLocation;



@interface BaseGPSViewController ()<CLLocationManagerDelegate>
@property (strong, nonatomic) CLLocationManager *locationManager;
@end

@implementation BaseGPSViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    [self.locationManager setDesiredAccuracy:kCLLocationAccuracyBestForNavigation];
    [self.locationManager setDistanceFilter:5];
    
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
}



-(void)startUpdate{
    
    [self.locationManager startUpdatingLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)verificaStatusGPS
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    // If the status is denied or only granted for when in use, display an alert
    if (status == kCLAuthorizationStatusDenied || ![CLLocationManager locationServicesEnabled]) {
        NSString *title = @"O GPS não está habilitado ou você não habilitou nosso aplicativo.";

        NSString *message = @"Para obtermos sua localização, você precisa habilita lo nas configurações";
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                                  otherButtonTitles:@"Configurações", nil];
        [alertView show];
        return NO;
    }
    
    return YES;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        // Send the user to the Settings for this app
        NSURL *settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:settingsURL];
    }
}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {

    if(!self.naoDesativaUpdate)
        [self.locationManager stopUpdatingLocation];
    
    self.currentLocation = locations[0];
    
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"AtualizaLocalizacao" object:nil];

}

-(void)obterCidadeUsuarioCompletionHandler:(void(^)(CLPlacemark *local,NSString *error))completionHandler{
    
    CLLocation *location;
    if(self.currentLocation)
        location = self.currentLocation;

    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:location
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       if (error){
                           NSLog(@"Geocode failed with error: %@", error);
                           completionHandler(nil,@"Ocorreu um erro ao obter sua cidade!");
                       }
                       else{
                           CLPlacemark *placemark = [placemarks objectAtIndex:0];
                           NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
                           completionHandler(placemark,nil);
                       }

                   }];
}


-(void)obterCidadeDigitada:(NSString *)cidadeDigitada completionHandler:(void(^)(NSArray *locais,NSString *error))completionHandler{



    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;

    [geocoder geocodeAddressString:cidadeDigitada completionHandler:^(NSArray *placemarks, NSError *error) {

        if (error){
            NSLog(@"Geocode failed with error: %@", error);
            completionHandler(@"",@"Ocorreu um erro ao obter sua cidade!");
        }
        else{

        NSLog(@"placemark.ISOcountryCode %@",placemarks);
            completionHandler(placemarks,nil);
        }


    }];

}



@end
