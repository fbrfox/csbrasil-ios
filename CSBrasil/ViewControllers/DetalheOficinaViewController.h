//
//  DetalheOficinaViewController.h
//  CSBrasil
//
//  Created by WIINC - Felipe on 10/10/16.
//  Copyright © 2016 CSBrasil. All rights reserved.
//

#import "BaseViewController.h"

@class Loja;


@interface DetalheOficinaViewController : BaseViewController

@property(nonatomic, strong) Loja *lojaSelecionada;

@end
