//
//  AgendamentoViewController.h
//  CSBrasil
//
//  Created by WIINC - Felipe on 14/09/16.
//  Copyright © 2016 CSBrasil. All rights reserved.
//

#import "BaseViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
@import MediaPlayer;
#import <UIKit/UIKit.h>


@interface AgendamentoViewController : BaseViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@end
