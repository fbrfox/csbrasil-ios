//
//  MapsViewController.m
//  CSBrasil
//
//  Created by WIINC - Felipe on 10/10/16.
//  Copyright © 2016 CSBrasil. All rights reserved.
//

#import "MapsViewController.h"
#import "Loja.h"
#import "UIHelper.h"

@interface MapsViewController ()
{
    IBOutlet GMSPanoramaView *pvLoja;
    IBOutlet GMSMapView *mapView;
    
    
}
@end

@implementation MapsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    if(!_showMaps) {
        
        [self.view sendSubviewToBack:mapView];
        [self.view bringSubviewToFront:pvLoja];
        [pvLoja moveNearCoordinate:CLLocationCoordinate2DMake([self.loja.latitude doubleValue], [self.loja.longitude doubleValue])];
        [pvLoja setAllGesturesEnabled:YES];
        
    }
    else{
        
        
        [[UIHelper sharedInstance] showProgress];
        
        [self.view sendSubviewToBack:pvLoja];
        [self.view bringSubviewToFront:mapView];
        
        
        [self mostraMinhaPosicao];
        [self mostrarLoja];
        [self mostrarRota];
        
    }
}

- (void)mostrarLoja {

    GMSMarker *startMarker = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake([self.loja.latitude doubleValue], [self.loja.longitude doubleValue])];
    startMarker.appearAnimation = kGMSMarkerAnimationPop;
    startMarker.title = self.loja.brand;
    startMarker.map = mapView;
    startMarker.icon = [UIImage imageNamed:@"pin"];
}

- (void)mostraMinhaPosicao {

    CLLocation *location = [[LocationTracker sharedInstance] myLastLocation];
    GMSMarker *startMarker = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(location.coordinate.latitude , location.coordinate.longitude )];
    startMarker.appearAnimation = kGMSMarkerAnimationPop;
    startMarker.title = @"Minha posição";
    startMarker.map = mapView;
    
    
}


- (void)mostrarRota
{


    CLLocation *destino = [[CLLocation alloc] initWithLatitude:[_loja.latitude doubleValue] longitude:[_loja.longitude doubleValue]];
    [self fetchPolylineWithOrigin:[LocationTracker sharedInstance].myLastLocation destination:destino completionHandler:^(GMSPath *path) {
        if (path){
            dispatch_async(dispatch_get_main_queue(), ^(void){

                //Traça a rota
                GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
                polyline.map = mapView;

                //Obtem o zoom para centralizar
                GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithPath:path];
                //There are several useful init methods for the GMSCoordinateBounds!
                GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds];
                [mapView moveCamera:update];
             

            });
            
            
           
        }
    }];
}

- (void)fetchPolylineWithOrigin:(CLLocation *)origin destination:(CLLocation *)destination completionHandler:(void (^)(GMSPath *))completionHandler
{


    NSString *originString = [NSString stringWithFormat:@"%f,%f", origin.coordinate.latitude, origin.coordinate.longitude];
    NSString *destinationString = [NSString stringWithFormat:@"%f,%f", destination.coordinate.latitude, destination.coordinate.longitude];
    NSString *directionsAPI = @"https://maps.googleapis.com/maps/api/directions/json?";
    NSString *directionsUrlString = [NSString stringWithFormat:@"%@&origin=%@&destination=%@&mode=driving&key=AIzaSyAgDj3X-zVdYBwQK39GV82vQo7kfaD-nUM", directionsAPI, originString, destinationString];
    NSURL *directionsUrl = [NSURL URLWithString:directionsUrlString];


    NSURLSessionDataTask *fetchDirectionsTask = [[NSURLSession sharedSession] dataTaskWithURL:directionsUrl completionHandler:
            ^(NSData *data, NSURLResponse *response, NSError *error)
            {
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                if(error)
                {
                    if(completionHandler)
                        completionHandler(nil);
                    return;
                }

                NSArray *routesArray = [json objectForKey:@"routes"];

                GMSPath *path = nil;
                if ([routesArray count] > 0)
                {
                    NSDictionary *routeDict = [routesArray objectAtIndex:0];
                    NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                    NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                    path = [GMSPath pathFromEncodedPath:points];
                 
                }

                if(completionHandler)
                    completionHandler(path);

                [[UIHelper sharedInstance] hideProgress];

            }];
    [fetchDirectionsTask resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
