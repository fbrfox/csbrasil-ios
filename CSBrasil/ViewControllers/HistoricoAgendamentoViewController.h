//
//  HistoricoAgendamentoViewController.h
//  CSBrasil
//
//  Created by WIINC - Felipe on 14/09/16.
//  Copyright © 2016 CSBrasil. All rights reserved.
//

#import "BaseViewController.h"

@interface HistoricoAgendamentoViewController : BaseViewController

@property(nonatomic, strong) NSArray *historicos;

@property(nonatomic) int source;
@end
