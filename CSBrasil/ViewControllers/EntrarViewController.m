//
//  EntrarViewController.m
//  CSBrasil
//
//  Created by WIINC - Felipe on 08/09/16.
//  Copyright © 2016 CSBrasil. All rights reserved.
//

#import "EntrarViewController.h"
#import "AppDelegate.h"

@interface EntrarViewController ()

@end

@implementation EntrarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addBackIcon];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [super viewWillAppear:animated];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
