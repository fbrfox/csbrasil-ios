    //
//  PlacarRenavamViewController.m
//  CSBrasil
//
//  Created by WIINC - Felipe on 12/09/16.
//  Copyright © 2016 CSBrasil. All rights reserved.
//

#import "PlacarRenavamViewController.h"
#import "AppDelegate.h"
#import "UIHelper.h"
#import "APIClient.h"
#import "ConsultaVeiculoRequest.h"
#import "ConsultaVeiculo.h"
#import "ConsultaVeiculoResponse.h"
#import "NSDefaultsHelper.h"

@interface PlacarRenavamViewController ()
{
    
    IBOutlet UITextField *tfPlaca;
    IBOutlet UITextField *tfRenavam;
    IBOutlet UISwitch *swLembrarPlaca;
}
@end

@implementation PlacarRenavamViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [super configdismissOnTap:@[tfPlaca,tfRenavam]];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onEntrar:(id)sender {
    
    if(![self validarCampos])
        return;

    ConsultaVeiculoRequest *request = [[ConsultaVeiculoRequest alloc] init];
    request.request = [[ConsultaVeiculo alloc] init];
    request.request.vehiclePlate = tfPlaca.text;
    request.request.vehicleRenavam = tfRenavam.text;


    [[UIHelper sharedInstance] showProgress];

    [[APIClient sharedManager] consultaVeiculo:request completion:^(ConsultaVeiculoResponse *response, NSString *errorMessage) {

        [[UIHelper sharedInstance] hideProgress];

        if(response.success){

            if(swLembrarPlaca.isOn)
                [NSDefaultsHelper lembrarVeiculo:YES];
                
            [NSDefaultsHelper salvarVeiculoAtual:response.veiculo];

            [DELEGATE showHome:self.storyboard];

        }
        else
            [[UIHelper sharedInstance] mostrarAlertaErro:response.message];
    }];


}

- (BOOL)validarCampos {

    if([tfPlaca.text isEqualToString:@""]){

        [[UIHelper sharedInstance] mostrarAlertaErro:@"Informe a placa"];
        [tfPlaca becomeFirstResponder];
        return NO;
    }
    else if([tfRenavam.text isEqualToString:@""])
    {
        [[UIHelper sharedInstance] mostrarAlertaErro:@"Informe o Renavam"];
        [tfRenavam becomeFirstResponder];
        return NO;
    }

    return YES;
}


@end
