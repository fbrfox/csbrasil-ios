//
//  HistoricoAgendamentoFiltroViewController.m
//  CSBrasil
//
//  Created by WIINC - Felipe on 11/10/16.
//  Copyright © 2016 CSBrasil. All rights reserved.
//

#import "HistoricoAgendamentoFiltroViewController.h"
#import "UIHelper.h"
#import "HistoricoAgendamentoViewController.h"
#import "NSDefaultsHelper.h"
#import "ListarAgendamentoRequest.h"
#import "ListarAgendamento.h"
#import "Veiculo.h"
#import "ListarAgendamentoResponse.h"
#import "APIClient.h"


@interface HistoricoAgendamentoFiltroViewController ()
{
    
    IBOutlet UITextField *tfDataDe;
    IBOutlet UITextField *tfDataAte;
    UIDatePicker *datePicker;
    NSDate *dataDeSelecionada;

    UIDatePicker *datePickerAte;
    NSDate *dataAteSelecionada;

}
@end

@implementation HistoricoAgendamentoFiltroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [super addLogoNavigation];
    [self initDatePicker];

    tfDataDe.text = [UIHelper adicionaDias:-1 data:[NSDate date] formato:@"dd/MM/yyyy HH:mm:ss"];
    tfDataAte.text = [UIHelper dataParaString:[NSDate date] formato:@"dd/MM/yyyy HH:mm:ss"];
    dataDeSelecionada = [[UIHelper sharedInstance] stringParaData:tfDataDe.text formato:@"dd/MM/yyyy HH:mm:ss"];
    dataAteSelecionada = [NSDate date];


}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)initDatePicker {

    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.tag = 1000;


    [datePicker addTarget:self action:@selector(updateTextField:)
         forControlEvents:UIControlEventValueChanged];

    [tfDataDe setInputView:datePicker];



    datePickerAte = [[UIDatePicker alloc] init];
    datePickerAte.datePickerMode = UIDatePickerModeDate;
    datePickerAte.tag = 1001;


    [datePickerAte addTarget:self action:@selector(updateTextField:)
         forControlEvents:UIControlEventValueChanged];

    [tfDataAte setInputView:datePickerAte];

}

- (void)updateTextField:(UIDatePicker *)picker {


    if (picker.tag == 1000) {

        tfDataDe.text = [UIHelper dataParaString:picker.date formato:@"dd/MM/yyyy HH:mm:ss"];

        dataDeSelecionada = datePicker.date;

    }
    else if(picker.tag == 1001){

        tfDataAte.text = [UIHelper dataParaString:picker.date formato:@"dd/MM/yyyy HH:mm:ss"];

        dataAteSelecionada = datePicker.date;

    }
}


- (IBAction)onFiltrarClick:(id)sender {

    if(![self validarCampos])
    {
        return;
    }


    ListarAgendamentoRequest *request = [self preencherRequest];

    [[UIHelper sharedInstance] showProgress];
    
    [[APIClient sharedManager] listarAgendamentos:request completion:^(ListarAgendamentoResponse *response, NSString *errorMessage) {
        
        [[UIHelper sharedInstance] hideProgress];
        
        if(!response.success){
            
            [[UIHelper sharedInstance] mostrarAlertaErro:response.message];
            
        }
        else{

            [self performSegueWithIdentifier:@"historicoSegue" sender:response.agendamentos];
        }
        
        
        
    }];
    

    
}

- (ListarAgendamentoRequest *)preencherRequest {
    Veiculo *veiculo = [NSDefaultsHelper obterVeiculoAtual];

    ListarAgendamentoRequest *request = [[ListarAgendamentoRequest alloc] init];
    request.request = [[ListarAgendamento alloc] init];
    request.request.initialScheduleDate = tfDataDe.text;
    request.request.finalScheduleDate = tfDataAte.text;
    request.request.vehiclePlate = veiculo.plate;
    return request;
}

- (BOOL)validarCampos {

    if(!dataDeSelecionada){
        [[UIHelper sharedInstance] mostrarAlertaErro:@"Selecione a data de"];
        return NO;
    }
    else if(!dataAteSelecionada){
        [[UIHelper sharedInstance] mostrarAlertaErro:@"Selecione a data até"];
        return NO;
    }
    else if([dataDeSelecionada laterDate:dataAteSelecionada] == dataDeSelecionada){

        [[UIHelper sharedInstance] mostrarAlertaErro:@"A data de deve ser menor que a data até"];
        return NO;
    }

    return YES;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {


    HistoricoAgendamentoViewController *historico = segue.destinationViewController;
    historico.historicos = sender;

    [super prepareForSegue:segue sender:sender];
}


@end
