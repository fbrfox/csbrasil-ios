//
//  AgendamentoViewController.m
//  CSBrasil
//
//  Created by WIINC - Felipe on 14/09/16.
//  Copyright © 2016 CSBrasil. All rights reserved.
//

#import "AgendamentoViewController.h"
#import "Veiculo.h"
#import "NSDefaultsHelper.h"
#import "UIHelper.h"
#import "APIClient.h"
#import "AgendarVistoriaRequest.h"
#import "Agendamento.h"
#import "LojaAgendarVistoria.h"
#import "Loja.h"
#import "UsuarioAgendarVistoria.h"
#import "VeiculoAgendarVistoria.h"
#import "AgendarVistoriaResponse.h"
#import "Foto.h"


@interface AgendamentoViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIActionSheetDelegate> {
    IBOutlet UICollectionView *cvImages;
    NSMutableArray *imagens;
    IBOutlet UITextField *tfKmAtual;
    IBOutlet UITextField *tfDDD;
    IBOutlet UITextField *tfCelular;
    IBOutlet UITextField *tfNome;
    IBOutlet UITextField *tfEmail;
    IBOutlet UITextField *tfPlaca;
    IBOutlet UITextField *tfData;
    IBOutlet UIButton *tfButton;
    IBOutlet UITextView *tfObservacao;
    IBOutlet UILabel *tfKmAnterior;
    Veiculo *veiculoAtual;
    UIDatePicker *datePicker;
    Loja *lojaSelecionada;


}
@end

@implementation AgendamentoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [super addLogoNavigation];
    
    
    [self initVariaveis];
    [self preencheCamposPadroes];
    [self configdismissOnTap:@[tfKmAtual, tfDDD, tfCelular, tfNome, tfEmail, tfPlaca, tfData, tfObservacao]];
    [self initDatePicker];


    
}

-(void)initVariaveis{
    
    imagens = [NSMutableArray new];
    veiculoAtual = [NSDefaultsHelper obterVeiculoAtual];
    //LOJA FAKE
    lojaSelecionada = [[Loja alloc] init];
    lojaSelecionada.id = @176;
}


- (void)initDatePicker {

    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    datePicker.tag = 1000;
    datePicker.minimumDate = [NSDate date];


    [datePicker addTarget:self action:@selector(updateTextField:)
         forControlEvents:UIControlEventValueChanged];

    [tfData setInputView:datePicker];
    tfData.tag = 1000;
}

- (void)updateTextField:(UIDatePicker *)datePickerr {

    if (datePickerr.tag == 1000) {

        tfData.text = [UIHelper dataParaString:datePicker.date formato:@"dd/MM/yyyy HH:mm:ss"];

    }
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}


- (void)preencheCamposPadroes {

    tfKmAnterior.text = [NSString stringWithFormat:@"%@", veiculoAtual.mileage];
    tfPlaca.text = veiculoAtual.plate;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return imagens.count + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    //    BannerCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[BannerCell cellIdentifier] forIndexPath:indexPath];

    if (imagens.count == 0 || indexPath.row == imagens.count) {
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellNovaFoto" forIndexPath:indexPath];

        return cell;

    }

    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellFoto" forIndexPath:indexPath];


    UIImageView *imageView = [cell viewWithTag:1];


    UIImage *image = imagens[indexPath.row];
    [imageView setImage:image];


    return cell;

}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.row == imagens.count) {

        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Selecione uma opção" delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:nil otherButtonTitles:@"Selecionar foto", @"Tirar foto", nil];


        sheet.tag = 1;

        [sheet showInView:self.view];

    }

}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {


    //    NSInteger itemCount = [bannerCollectionViews numberOfItemsInSection:0];
    //    NSInteger pages = ceil(itemCount / 16.0);

    return CGSizeMake(90, 90);


}


- (void)chooseFoto:(UIImagePickerControllerSourceType)source {
    //Tira a foto
    if (![UIImagePickerController isSourceTypeAvailable:source]) {
        NSLog(@"Camera not Available");
        return;
    }

    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker setSourceType:source];
    [imagePicker setMediaTypes:@[(NSString *) kUTTypeImage]];
    [imagePicker setAllowsEditing:YES];
    [imagePicker setDelegate:self];

    [self presentViewController:imagePicker animated:YES completion:nil];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {

    if (actionSheet.tag == 1) {


        if (buttonIndex == 0) {
            [self chooseFoto:UIImagePickerControllerSourceTypePhotoLibrary];
        }
        else if (buttonIndex == 1) {
            [self chooseFoto:UIImagePickerControllerSourceTypeCamera];
        }
    }

}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *, id> *)info {


    UIImage *originalImage, *editedImage, *imageToSave;

    originalImage = info[UIImagePickerControllerOriginalImage];
    editedImage = info[UIImagePickerControllerEditedImage];


    if (editedImage) {
        imageToSave = editedImage;
    }
    else {
        imageToSave = originalImage;
    }

    [imagens addObject:imageToSave];

    [cvImages reloadData];

    [self dismissViewControllerAnimated:YES completion:^{

    }];
}

- (IBAction)onEnviarClick:(id)sender {


    if (![self validarCampos])
        return;


    [[UIHelper sharedInstance] showProgress];

    AgendarVistoriaRequest *request = [self preencheRequest];

    [[APIClient sharedManager] agendarVistoria:request completion:^(AgendarVistoriaResponse *response, NSString *errorMessage) {

        if (response.success) {

            veiculoAtual.mileage = @([request.request.kilometer intValue]);
            [NSDefaultsHelper salvarVeiculoAtual:veiculoAtual];
            [[UIHelper sharedInstance] hideProgress];
            [[UIHelper sharedInstance] mostrarAlerta:response.message];
            [self limparCampos];

            [self preencheCamposPadroes];
        }
        else {
            [[UIHelper sharedInstance] hideProgress];
            [[UIHelper sharedInstance] mostrarAlertaErro:response.message];
        }
    }];


}

- (void)limparCampos {


    tfKmAtual.text = @"";
    tfDDD.text = @"";
    tfCelular.text = @"";
    tfNome.text = @"";
    tfEmail.text = @"";
    tfData.text = @"";
    imagens = [[NSMutableArray alloc] init];
    [cvImages reloadData];
    tfObservacao.text = @"Observação";

}

- (AgendarVistoriaRequest *)preencheRequest {
    AgendarVistoriaRequest *request = [[AgendarVistoriaRequest alloc] init];
    Agendamento *agendamento = [[Agendamento alloc] init];
    request.request = agendamento;

    agendamento.alreadyInTheWorkshop = @NO;
    agendamento.attribute = @"teste mobile";
    agendamento.authorizedWorkshop = [[LojaAgendarVistoria alloc] initWithId:lojaSelecionada.id];
    agendamento.driverEmail = tfEmail.text;
    agendamento.driverName = tfNome.text;
    agendamento.employeeName = @"Teste Mobile";
    agendamento.kilometer = tfKmAtual.text;


    CLLocation *location = [LocationTracker sharedInstance].myLastLocation;
    if (location) {


        agendamento.latitude = @(location.coordinate.latitude);
        agendamento.longitude = @(location.coordinate.longitude);
    }

    agendamento.phone = [tfDDD.text stringByAppendingFormat:@"%@", tfCelular.text];
    agendamento.photoList = [[NSArray alloc] init];
    agendamento.observation = tfObservacao.text;


    NSTimeInterval dataInterval = [[[UIHelper sharedInstance] stringParaData:tfData.text formato:@"dd/MM/yyyy HH:mm:ss"] timeIntervalSinceReferenceDate];
    agendamento.scheduleDate = [NSString stringWithFormat:@"%li", (long) dataInterval];

    agendamento.registrationDate = [NSString stringWithFormat:@"%li", (long) [[NSDate date] timeIntervalSinceReferenceDate]];
    agendamento.status = @"SCHEDULED";
    agendamento.type = @"CORRECTIVE";
    agendamento.user = [[UsuarioAgendarVistoria alloc] initWithCpf:@"37357750809" nome:@"Pedro Henrique"];
    agendamento.vehicle = [[VeiculoAgendarVistoria alloc] initWithPlate:veiculoAtual.plate renavam:veiculoAtual.renavam];


    NSMutableArray<Foto> *fotos = (NSMutableArray <Foto> *) [[NSMutableArray alloc] initWithCapacity:imagens.count];


    for (UIImage *image in imagens) {
        UIImage *photo = [[UIHelper sharedInstance] imageWithImage:image scaledToSize:CGSizeMake(120,120)];
        NSString *imageToNSString = [[UIHelper sharedInstance] imageToNSString:photo];
        [fotos addObject:[Foto fotoWithPhoto:imageToNSString]];
    }

    agendamento.photoList = fotos;

    return request;
}

- (BOOL)validarCampos {


    if ([tfKmAtual.text isEqualToString:@""]) {

        [[UIHelper sharedInstance] mostrarAlertaErro:@"Informe o KM Atual"];
        [tfKmAtual becomeFirstResponder];
        return NO;
    }
    else if ([tfKmAtual.text intValue] < [tfKmAnterior.text intValue]) {

        [[UIHelper sharedInstance] mostrarAlertaErro:@"A Kilometragem atual é menor que a Kilometragem anterior!"];
        [tfKmAtual becomeFirstResponder];
        return NO;
    }
    else if ([tfDDD.text isEqualToString:@""]) {
        [[UIHelper sharedInstance] mostrarAlertaErro:@"Informe o DDD"];
        [tfDDD becomeFirstResponder];
        return NO;
    }
    else if ([tfCelular.text isEqualToString:@""]) {
        [[UIHelper sharedInstance] mostrarAlertaErro:@"Informe o Celular"];
        [tfCelular becomeFirstResponder];
        return NO;
    }
    else if ([tfNome.text isEqualToString:@""]) {

        [[UIHelper sharedInstance] mostrarAlertaErro:@"Informe o Nome"];
        [tfNome becomeFirstResponder];
        return NO;
    }
    else if ([tfEmail.text isEqualToString:@""]) {

        [[UIHelper sharedInstance] mostrarAlertaErro:@"Informe o E-Mail"];
        [tfEmail becomeFirstResponder];
        return NO;
    }
    else if ([tfData.text isEqualToString:@""]) {

        [[UIHelper sharedInstance] mostrarAlertaErro:@"Informe a Data do Agendamento"];
        [tfData becomeFirstResponder];
        return NO;
    }


    return YES;
}

@end
