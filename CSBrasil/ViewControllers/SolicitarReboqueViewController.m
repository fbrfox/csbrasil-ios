//
//  SolicitarReboqueViewController.m
//  CSBrasil
//
//  Created by WIINC - Felipe on 14/09/16.
//  Copyright © 2016 CSBrasil. All rights reserved.
//

#import "SolicitarReboqueViewController.h"
#import "UIHelper.h"
#import "AgendarVistoriaRequest.h"
#import "Agendamento.h"
#import "LojaAgendarVistoria.h"
#import "Loja.h"
#import "Veiculo.h"
#import "NSDefaultsHelper.h"
#import "APIClient.h"
#import "AgendarVistoriaResponse.h"
#import "VeiculoAgendarVistoria.h"
#import "UsuarioAgendarVistoria.h"

@interface SolicitarReboqueViewController ()
{
    
    IBOutlet UITextField *tfNome;
    IBOutlet UITextField *tfEmail;
    IBOutlet UITextField *tfDDD;
    IBOutlet UITextField *tfCelular;
    IBOutlet UITextField *tfData;
    IBOutlet UITextView *tfObservacao;
    Loja *lojaSelecionada;
    Veiculo *veiculoAtual;
    UIDatePicker *datePicker;
    IBOutlet UITextField *tfKm;
}
@end

@implementation SolicitarReboqueViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [super addLogoNavigation];

    [self configdismissOnTap:@[tfNome,tfEmail,tfDDD,tfCelular,tfObservacao]];

    veiculoAtual = [NSDefaultsHelper obterVeiculoAtual];
    //LOJA FAKE
    lojaSelecionada = [[Loja alloc] init];
    lojaSelecionada.id = @176;

    tfKm.text = [NSString stringWithFormat:@"%@", veiculoAtual.mileage];


}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    tfData.text = [UIHelper dataParaString:[NSDate date] formato:@"dd/MM/yyyy HH:mm"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)onSolicitarClick:(id)sender {

    if (![self validarCampos])
        return;


    [[UIHelper sharedInstance] showProgress];

    AgendarVistoriaRequest *request = [self preencheRequest];

    [[APIClient sharedManager] solicitarReboque:request completion:^(AgendarVistoriaResponse *response, NSString *errorMessage) {

        [[UIHelper sharedInstance] hideProgress];

        if (response.success) {

            [[UIHelper sharedInstance] mostrarAlerta:response.message];
            [self limparCampos];
        }
        else
            [[UIHelper sharedInstance] mostrarAlertaErro:response.message];
    }];


}

- (void)limparCampos {

    tfDDD.text = @"";
    tfCelular.text = @"";
    tfNome.text = @"";
    tfEmail.text = @"";
    tfObservacao.text = @"Observação";

}

- (AgendarVistoriaRequest *)preencheRequest {
    AgendarVistoriaRequest *request = [[AgendarVistoriaRequest alloc] init];
    Agendamento *agendamento = [[Agendamento alloc] init];
    request.request = agendamento;

    agendamento.alreadyInTheWorkshop = @NO;
    agendamento.attribute = @"teste mobile";
    agendamento.authorizedWorkshop = [[LojaAgendarVistoria alloc] initWithId:lojaSelecionada.id];
    agendamento.driverEmail = tfEmail.text;
    agendamento.driverName = tfNome.text;
    agendamento.employeeName = @"Teste Mobile";
    agendamento.kilometer = tfKm.text;


    CLLocation *location = [LocationTracker sharedInstance].myLastLocation;
    if (location) {


        agendamento.latitude = @(location.coordinate.latitude);
        agendamento.longitude = @(location.coordinate.longitude);
    }

    agendamento.phone = [tfDDD.text stringByAppendingFormat:@"%@", tfCelular.text];
    agendamento.photoList = [[NSArray alloc] init];
    agendamento.observation = tfObservacao.text;


    NSTimeInterval dataInterval = [[[UIHelper sharedInstance] stringParaData:tfData.text formato:@"dd/MM/yyyy HH:mm:ss"] timeIntervalSinceReferenceDate];
    agendamento.scheduleDate = [NSString stringWithFormat:@"%li", (long) dataInterval];

    agendamento.registrationDate = [NSString stringWithFormat:@"%li", (long) [[NSDate date] timeIntervalSinceReferenceDate]];
    agendamento.status = @"SCHEDULED";
    agendamento.type = @"CORRECTIVE";
    agendamento.user = [[UsuarioAgendarVistoria alloc] initWithCpf:@"37357750809" nome:@"Pedro Henrique"];
    agendamento.vehicle = [[VeiculoAgendarVistoria alloc] initWithPlate:veiculoAtual.plate renavam:veiculoAtual.renavam];


    return request;
}

- (BOOL)validarCampos {



    if ([tfDDD.text isEqualToString:@""]) {
        [[UIHelper sharedInstance] mostrarAlertaErro:@"Informe o DDD"];
        [tfDDD becomeFirstResponder];
        return NO;
    }
    else if ([tfCelular.text isEqualToString:@""]) {
        [[UIHelper sharedInstance] mostrarAlertaErro:@"Informe o Celular"];
        [tfCelular becomeFirstResponder];
        return NO;
    }
    else if ([tfNome.text isEqualToString:@""]) {

        [[UIHelper sharedInstance] mostrarAlertaErro:@"Informe o Nome"];
        [tfNome becomeFirstResponder];
        return NO;
    }
    else if ([tfEmail.text isEqualToString:@""]) {

        [[UIHelper sharedInstance] mostrarAlertaErro:@"Informe o E-Mail"];
        [tfEmail becomeFirstResponder];
        return NO;
    }
    else if ([tfData.text isEqualToString:@""]) {

        [[UIHelper sharedInstance] mostrarAlertaErro:@"Informe a Data"];
        [tfData becomeFirstResponder];
        return NO;
    }
    else if ([tfObservacao.text isEqualToString:@""]) {

        [[UIHelper sharedInstance] mostrarAlertaErro:@"Informe a Observação"];
        [tfObservacao becomeFirstResponder];
        return NO;
    }


    return YES;
}
@end
