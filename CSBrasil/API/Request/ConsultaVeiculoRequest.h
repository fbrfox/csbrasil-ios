//
// Created by WIINC - Felipe on 04/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ConsultaVeiculo;


@interface ConsultaVeiculoRequest : RequestBase

@property(nonatomic, strong) ConsultaVeiculo *request;

@end