//
// Created by WIINC - Felipe on 14/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RegistrarEntrada;


@interface RegistrarEntradaRequest : RequestBase

@property(nonatomic, strong) RegistrarEntrada *request;

@end