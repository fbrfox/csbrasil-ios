//
// Created by WIINC - Felipe on 11/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ListarAgendamento;


@interface ListarAgendamentoRequest : RequestBase

@property(nonatomic, strong) ListarAgendamento *request;

@end