//
// Created by WIINC - Felipe on 05/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VeiculoAgendarVistoria;
@class LojaAgendarVistoria;
@class UsuarioAgendarVistoria;
@class Agendamento;


/*
 *
 * {
  "token":"JSLmobile.2014H",
  "request":
  {

"id":0,
       "scheduleDate":850864846565,
       "entranceDate":0,
       "exitDate":0,
       "conclusionDate":0,
       "registrationDate":0,
       "phone":"4999101884",
       "photoList":[],
       "observation":"TESTE INTEGRAÇÂO",
       "kilometer":"48659",
       "type":"CORRECTIVE",
		"vehicle": {
         "plate":"HIE4683",
         "renavam":"301797447"
       },
       "authorizedWorkshop":
       {
		"id":25

       },
       "alreadyInTheWorkshop":false,
       "attribute":"Peça x, Sintoma y, Posição z",
       "user":
       {
         "cpf":"06885270946",
         "nome":"TESTE"
       },
		"latitude":"-46.3278",
		"longitude":"-46.3278",
		"employeeName":"MOB_APP_TESTE",
		"status":"SCHEDULED",
       "driverName":"TESTE",
       "driverEmail":"teste@wlmsystems.com.br"
     }
  }

 *
 * */

@interface AgendarVistoriaRequest : RequestBase


@property(nonatomic, strong) Agendamento *request;


@end