//
// Created by pedro henrique borges on 3/5/15.
// Copyright (c) 2015 Iterative. All rights reserved.
//



#import "APIClient.h"
#import "ConsultaVeiculoRequest.h"
#import "ConsultaVeiculoResponse.h"
#import "NSDefaultsHelper.h"
#import "UIHelper.h"
#import "AgendarVistoriaRequest.h"
#import "AgendarVistoriaResponse.h"
#import "ListarAgendamentoRequest.h"
#import "ListarAgendamentoResponse.h"
#import "RegistrarEntradaRequest.h"


@class Usuario;


//EndPoints Urls
//PRODUCAO
//NSString *const APIBaseURL = @"https://apihomologacao.movida.com.br/";

//HOMOLOG
NSString *const APIBaseURL = @"http://jslfreteh.jsl.com.br:8080/jsllocacao/rest/";

//DEV
//NSString *const APIBaseURL = @"http://192.168.0.15/taquiisafe/v1/";
NSString *const APIConsultaVeiculo = @"vehicle/load";
NSString *const APIAgendarVistoria= @"scheduling/insert";
NSString *const APISolicitarReboque= @"scheduling/inserttrailerrequest";
NSString *const APIListarAgendamentos= @"scheduling/load";
NSString *const APIRegistrarEntrada= @"vehicleentrance/insert";
NSString *const APIRegistrarSaida= @"scheduling/insertvehicleexit";








// Errors
int const kErrorUnauthorized = 401;


//Keys
NSString *const KeyUserToken = @"KeyUserToken";
NSString *const KeyUserID = @"KeyUserID";
NSString *const KeyUserCPF = @"KeyUserCPF";;
NSString *const headerUserToken = @"Authorization";
NSString *const headerPlataforma= @"Plataforma";
NSString *const headerDeviceToken= @"DeviceToken";

//ApiToken
NSString *apiToken = @"8fc8714e4514d3f015210573990ebef5";

//Localized Strings
NSString *const serverDown = @"ServerDown";
NSString *const badCredentials = @"BadCredentials";


@implementation APIClient


#pragma mark Util


#pragma mark Autenticação




+ (BOOL)autenticado {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:KeyUserToken]) {
        return YES;
    } else {
        return NO;
    }
}

- (void)authenticateWithToken:(NSString *)token {
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:KeyUserToken];


}

- (void)logout {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KeyUserToken];

    [[NSUserDefaults standardUserDefaults] synchronize];
//
//    [self.requestSerializer setValue:@"" forHTTPHeaderField:headerUserToken];
//    [APIClient configClient:[APIClient sharedManager]];
}

#pragma mark Configurações Header




#pragma mark Singleton


- (instancetype)init {
 
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    self = [[APIClient alloc] initWithBaseURL:[NSURL URLWithString:APIBaseURL] sessionConfiguration:configuration];
    if(self){

        [self setRequestSerializer:[AFJSONRequestSerializer serializer]];
        [self setResponseSerializer:[AFJSONRequestSerializer serializer]];
        [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];


    }
    return self;

}

+ (APIClient *)sharedManager {
    static APIClient *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];

        _sharedManager = [[APIClient alloc] initWithBaseURL:[NSURL URLWithString:APIBaseURL] sessionConfiguration:configuration];
        [self configClient:_sharedManager];
    });
    
    
    return _sharedManager;
}


+ (void)configClient:(APIClient *)_sharedManager {
    [_sharedManager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [_sharedManager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [_sharedManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [_sharedManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];

}






- (NSURLSessionDataTask *)consultaVeiculo:(ConsultaVeiculoRequest *)request completion:(void (^)(ConsultaVeiculoResponse *response, NSString *errorMessage))completion {
    
    NSDictionary *parametros = [request toDictionary];
    
    
    NSURLSessionDataTask *task = [self POSTWithLogParams:APIConsultaVeiculo parameters:parametros
                                              success:^(NSURLSessionDataTask *task, id responseObject) {
                                                  
                                                  NSError *error;
                                                  ConsultaVeiculoResponse *response = [[ConsultaVeiculoResponse alloc] initWithDictionary:responseObject error:&error];
                                                  
                                                  if (error)
                                                    [[UIHelper sharedInstance] mostrarAlertaErro:NSLocalizedString(serverDown, nil)];
                                                  else {
                                                      completion(response, nil);
                                                  }
                                                  
                                                  
                                              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                  
                                                  if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                                                      completion(nil, NSLocalizedString(badCredentials, nil));
                                                  } else {
                                                      completion(nil, NSLocalizedString(serverDown, nil));
                                                  }
                                              }];
    
    return task;
}


- (NSURLSessionDataTask *)agendarVistoria:(AgendarVistoriaRequest *)request completion:(void (^)(AgendarVistoriaResponse *response, NSString *errorMessage))completion {

    NSDictionary *parametros = [request toDictionary];


    NSURLSessionDataTask *task = [self POSTWithOutLog:APIAgendarVistoria parameters:parametros
                                                 success:^(NSURLSessionDataTask *task, id responseObject) {

                                                     NSError *error;
                                                     AgendarVistoriaResponse *response = [[AgendarVistoriaResponse alloc] initWithDictionary:responseObject error:&error];

                                                     if (error)
                                                         [[UIHelper sharedInstance] mostrarAlertaErro:NSLocalizedString(serverDown, nil)];
                                                     else {
                                                         completion(response, nil);
                                                     }


                                                 } failure:^(NSURLSessionDataTask *task, NSError *error) {

                if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                    completion(nil, NSLocalizedString(badCredentials, nil));
                } else {
                    completion(nil, NSLocalizedString(serverDown, nil));
                }
            }];

    return task;
}



- (NSURLSessionDataTask *)solicitarReboque:(AgendarVistoriaRequest *)request completion:(void (^)(AgendarVistoriaResponse *response, NSString *errorMessage))completion {

    NSDictionary *parametros = [request toDictionary];


    NSURLSessionDataTask *task = [self POSTWithOutLog:APISolicitarReboque parameters:parametros
                                              success:^(NSURLSessionDataTask *task, id responseObject) {

                                                  NSError *error;
                                                  AgendarVistoriaResponse *response = [[AgendarVistoriaResponse alloc] initWithDictionary:responseObject error:&error];

                                                  if (error)
                                                      [[UIHelper sharedInstance] mostrarAlertaErro:NSLocalizedString(serverDown, nil)];
                                                  else {
                                                      completion(response, nil);
                                                  }


                                              } failure:^(NSURLSessionDataTask *task, NSError *error) {

                if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                    completion(nil, NSLocalizedString(badCredentials, nil));
                } else {
                    completion(nil, NSLocalizedString(serverDown, nil));
                }
            }];

    return task;
}



- (NSURLSessionDataTask *)listarAgendamentos:(ListarAgendamentoRequest *)request completion:(void (^)(ListarAgendamentoResponse *response, NSString *errorMessage))completion {

    NSDictionary *parametros = [request toDictionary];


    NSURLSessionDataTask *task = [self POSTWithLogParams:APIListarAgendamentos parameters:parametros
                                              success:^(NSURLSessionDataTask *task, id responseObject) {

                                                  NSError *error;
                                                  ListarAgendamentoResponse *response = [[ListarAgendamentoResponse alloc] initWithDictionary:responseObject error:&error];

                                                  if (error)
                                                      [[UIHelper sharedInstance] mostrarAlertaErro:NSLocalizedString(serverDown, nil)];
                                                  else {
                                                      completion(response, nil);
                                                  }


                                              } failure:^(NSURLSessionDataTask *task, NSError *error) {

                if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                    completion(nil, NSLocalizedString(badCredentials, nil));
                } else {
                    completion(nil, NSLocalizedString(serverDown, nil));
                }
            }];

    return task;
}



- (NSURLSessionDataTask *)registrarEntrada:(RegistrarEntradaRequest *)request completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion {

    NSDictionary *parametros = [request toDictionary];


    NSURLSessionDataTask *task = [self POSTWithLogParams:APIRegistrarEntrada parameters:parametros
                                                 success:^(NSURLSessionDataTask *task, id responseObject) {

                                                     NSError *error;
                                                     ResponseBase *response = [[ResponseBase alloc] initWithDictionary:responseObject error:&error];

                                                     if (error)
                                                         [[UIHelper sharedInstance] mostrarAlertaErro:NSLocalizedString(serverDown, nil)];
                                                     else {
                                                         completion(response, nil);
                                                     }


                                                 } failure:^(NSURLSessionDataTask *task, NSError *error) {

                if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                    completion(nil, NSLocalizedString(badCredentials, nil));
                } else {
                    completion(nil, NSLocalizedString(serverDown, nil));
                }
            }];

    return task;
}


- (NSURLSessionDataTask *)registrarSaida:(AgendarVistoriaRequest *)request completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion {

    NSDictionary *parametros = [request toDictionary];


    NSURLSessionDataTask *task = [self POSTWithLogParams:APIRegistrarSaida parameters:parametros
                                                 success:^(NSURLSessionDataTask *task, id responseObject) {

                                                     NSError *error;
                                                     ResponseBase *response = [[ResponseBase alloc] initWithDictionary:responseObject error:&error];

                                                     if (error)
                                                         [[UIHelper sharedInstance] mostrarAlertaErro:NSLocalizedString(serverDown, nil)];
                                                     else {
                                                         completion(response, nil);
                                                     }


                                                 } failure:^(NSURLSessionDataTask *task, NSError *error) {

                if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                    completion(nil, NSLocalizedString(badCredentials, nil));
                } else {
                    completion(nil, NSLocalizedString(serverDown, nil));
                }
            }];

    return task;
}



- (NSURLSessionDataTask *)POSTWithOutLog:(NSString *)URLString
                                 parameters:(NSDictionary *)parameters
                                    success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                    failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {



    NSURLSessionDataTask *task = [self POST:URLString
                                 parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {

                NSLog(@"Response Dictionary :%@", responseObject);

                success(task, responseObject);
            }                       failure:^(NSURLSessionDataTask *task, NSError *error) {


#if DEBUG
                NSLog(@"%@", error);
#endif

                failure(task, error);
            }];

    return task;

}


//Metodo que encapsula o método de POST somente para logar os parametros em desenvolvimento
- (NSURLSessionDataTask *)POSTWithLogParams:(NSString *)URLString
                                 parameters:(NSDictionary *)parameters
                                    success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                    failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {


#if DEBUG
    NSError *errorAutentication;
    NSData *jsonDataAutentication = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&errorAutentication];
    if (!errorAutentication) {
        NSLog(@"POST URL :%@ >>>>>>>>>>>>> Json params:%@", URLString, [[NSString alloc] initWithData:jsonDataAutentication encoding:NSUTF8StringEncoding]);
    }
#endif


    NSURLSessionDataTask *task = [self POST:URLString
                                 parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {

                NSLog(@"Response Dictionary :%@", responseObject);

                success(task, responseObject);
            }                       failure:^(NSURLSessionDataTask *task, NSError *error) {


#if DEBUG
                NSLog(@"%@", error);
#endif

                failure(task, error);
            }];

    return task;

}

//Metodo que encapsula o método de PUT somente para logar os parametros em desenvolvimento
- (NSURLSessionDataTask *)PUTWithLogParams:(NSString *)URLString
                                parameters:(NSDictionary *)parameters
                                   success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                   failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {


#if DEBUG
    NSError *errorAutentication;
    NSData *jsonDataAutentication = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&errorAutentication];
    if (!errorAutentication) {
        NSLog(@"PUT URL :%@ >>>>>>>>>>>>> Json params:%@", URLString, [[NSString alloc] initWithData:jsonDataAutentication encoding:NSUTF8StringEncoding]);
    }
#endif


    NSURLSessionDataTask *task = [self PUT:URLString
                                parameters:parameters
                                   success:^(NSURLSessionDataTask *task, id responseObject) {
                                       NSLog(@"Response Dictionary :%@", responseObject);
                                       success(task, responseObject);
                                   } failure:^(NSURLSessionDataTask *task, NSError *error) {

#if DEBUG
                NSLog(@"%@", error);
#endif

                failure(task, error);
            }];

    return task;

}


- (NSURLSessionDataTask *)GETURLFormatedWithLogParams:(NSString *)URLString
                                           parameters:(NSDictionary *)parameters
                                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {


#if DEBUG
    NSError *errorAutentication;
    NSData *jsonDataAutentication = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&errorAutentication];
    if (!errorAutentication) {
        NSLog(@"GET URL :%@ >>>>>>>>>>>>> Json params:%@", URLString, [[NSString alloc] initWithData:jsonDataAutentication encoding:NSUTF8StringEncoding]);
    }
#endif

    NSURLSessionDataTask *task = [self GET:URLString
                                parameters:nil
                                   success:^(NSURLSessionDataTask *task, id responseObject) {
                                       NSLog(@"Response Dictionary :%@", responseObject);
                                       success(task, responseObject);
                                   }
                                   failure:^(NSURLSessionDataTask *task, NSError *error) {

#if DEBUG
                                       NSLog(@"%@", error);
#endif

                                       failure(task, error);
                                   }];

    return task;

}

//Metodo que encapsula o método de Get somente para logar os parametros em desenvolvimento
- (NSURLSessionDataTask *)GETWithLogParams:(NSString *)URLString
                                parameters:(NSDictionary *)parameters
                                   success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                   failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {


    for (NSString *key in parameters.allKeys) {

        URLString = [URLString stringByAppendingString:[NSString stringWithFormat:@"/%@/%@", key, parameters[key]]];

    }

#if DEBUG
    NSError *errorAutentication;
    NSData *jsonDataAutentication = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&errorAutentication];
    if (!errorAutentication) {
        NSLog(@"GET URL :%@ >>>>>>>>>>>>> Json params:%@", URLString, [[NSString alloc] initWithData:jsonDataAutentication encoding:NSUTF8StringEncoding]);
    }
#endif

    NSURLSessionDataTask *task = [self  GET:URLString
                                parameters:nil
                                   success:^(NSURLSessionDataTask *task, id responseObject) {
                                       NSLog(@"Response Dictionary :%@", responseObject);
                                       success(task, responseObject);
                                   }
                                   failure:^(NSURLSessionDataTask *task, NSError *error) {

#if DEBUG
                                       NSLog(@"%@", error);
#endif

                                       failure(task, error);
                                   }];

    return task;

}

- (void)setUsuario:(NSString *)cpf {

    [[NSUserDefaults standardUserDefaults] setObject:cpf forKey:KeyUserCPF];
}

+ (NSString *)getUsuario {

    return [[NSUserDefaults standardUserDefaults] objectForKey:KeyUserCPF];
}
@end