//
// Created by WIINC - Felipe on 05/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import "AgendarVistoriaResponse.h"


@implementation AgendarVistoriaResponse {

}

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
            @"object": @"idVistoria",
    }];
}
@end