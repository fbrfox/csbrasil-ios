//
// Created by WIINC - Felipe on 04/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Veiculo;


@interface ConsultaVeiculoResponse : ResponseBase

@property(nonatomic, strong) Veiculo *veiculo;

@end