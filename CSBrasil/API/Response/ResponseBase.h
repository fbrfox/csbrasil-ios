//
// Created by Movida on 20/08/15.
// Copyright (c) 2015 Edouard Roussillon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JSONModel/JSONModel.h>


@interface ResponseBase : JSONModel

@property (assign, nonatomic) BOOL success;
@property (strong, nonatomic) NSString* message;




@end