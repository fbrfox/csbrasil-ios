//
// Created by WIINC - Felipe on 04/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import "ConsultaVeiculoResponse.h"
#import "Veiculo.h"


@implementation ConsultaVeiculoResponse {

}


+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
            @"object": @"veiculo",
    }];
}
@end