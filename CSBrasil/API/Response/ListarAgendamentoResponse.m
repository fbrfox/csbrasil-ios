//
// Created by WIINC - Felipe on 11/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import "ListarAgendamentoResponse.h"
#import "HistoricoAgendamento.h"


@implementation ListarAgendamentoResponse {

}

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
            @"object": @"agendamentos",
    }];
}

@end