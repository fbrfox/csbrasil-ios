//
// Created by WIINC - Felipe on 05/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AgendarVistoriaResponse : ResponseBase

@property(nonatomic, strong) NSNumber *idVistoria;
@end