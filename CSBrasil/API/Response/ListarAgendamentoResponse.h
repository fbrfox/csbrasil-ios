//
// Created by WIINC - Felipe on 11/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HistoricoAgendamento;


@interface ListarAgendamentoResponse : ResponseBase

@property(nonatomic, strong) NSArray<HistoricoAgendamento> *agendamentos;

@end