//
// Created by WIINC - Felipe on 05/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UsuarioAgendarVistoria : JSONModel

@property(nonatomic, strong) NSString *cpf;
@property(nonatomic, strong) NSString *nome;

- (instancetype)initWithCpf:(NSString *)cpf nome:(NSString *)nome;

+ (instancetype)vistoriaWithCpf:(NSString *)cpf nome:(NSString *)nome;


@end