//
// Created by WIINC - Felipe on 05/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import "VeiculoAgendarVistoria.h"


@implementation VeiculoAgendarVistoria {

}
- (instancetype)initWithPlate:(NSString *)plate renavam:(NSString *)renavam {
    self = [super init];
    if (self) {
        self.plate = plate;
        self.renavam = renavam;
    }

    return self;
}

+ (instancetype)vistoriaWithPlate:(NSString *)plate renavam:(NSString *)renavam {
    return [[self alloc] initWithPlate:plate renavam:renavam];
}

@end