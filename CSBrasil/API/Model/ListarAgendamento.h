//
// Created by WIINC - Felipe on 11/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ListarAgendamento : JSONModel

@property(nonatomic, strong) NSNumber *id;
@property(nonatomic, strong) NSString *finalScheduleDate;
@property(nonatomic, strong) NSString *initialScheduleDate;
@property(nonatomic, strong) NSString *vehiclePlate;



@end