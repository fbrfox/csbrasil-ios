//
// Created by WIINC - Felipe on 05/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import "UsuarioAgendarVistoria.h"


@implementation UsuarioAgendarVistoria {

}
- (instancetype)initWithCpf:(NSString *)cpf nome:(NSString *)nome {
    self = [super init];
    if (self) {
        self.cpf = cpf;
        self.nome = nome;
    }

    return self;
}

+ (instancetype)vistoriaWithCpf:(NSString *)cpf nome:(NSString *)nome {
    return [[self alloc] initWithCpf:cpf nome:nome];
}

@end