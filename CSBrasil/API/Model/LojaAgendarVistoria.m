//
// Created by WIINC - Felipe on 05/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import "LojaAgendarVistoria.h"


@implementation LojaAgendarVistoria {

}
- (instancetype)initWithId:(NSNumber *)id {
    self = [super init];
    if (self) {
        self.id = id;
    }

    return self;
}

+ (instancetype)vistoriaWithId:(NSNumber *)id {
    return [[self alloc] initWithId:id];
}

@end