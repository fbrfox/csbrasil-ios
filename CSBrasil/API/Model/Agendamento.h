//
// Created by WIINC - Felipe on 05/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LojaAgendarVistoria;
@class VeiculoAgendarVistoria;
@class UsuarioAgendarVistoria;
@protocol Foto;


@interface Agendamento : JSONModel

@property(nonatomic, strong) NSNumber *id;
@property(nonatomic, strong) NSString *scheduleDate;
@property(nonatomic, strong) NSString *entranceDate;
@property(nonatomic, strong) NSString *exitDate;
@property(nonatomic, strong) NSString *conclusionDate;
@property(nonatomic, strong) NSString *registrationDate;
@property(nonatomic, strong) NSString *phone;
@property(nonatomic, strong) NSArray<Foto> *photoList;
@property(nonatomic, strong) NSString *observation;
@property(nonatomic, strong) NSString *kilometer;
@property(nonatomic, strong) NSString *type;
@property(nonatomic, strong) VeiculoAgendarVistoria *vehicle;
@property(nonatomic, strong) NSString *attribute;
@property(nonatomic, strong) LojaAgendarVistoria *authorizedWorkshop;
@property(nonatomic, strong) NSNumber *alreadyInTheWorkshop;
@property(nonatomic, strong) UsuarioAgendarVistoria *user;
@property(nonatomic, strong) NSNumber *latitude;
@property(nonatomic, strong) NSNumber *longitude;
@property(nonatomic, strong) NSString *employeeName;
@property(nonatomic, strong) NSString *status;
@property(nonatomic, strong) NSString *driverName;
@property(nonatomic, strong) NSString *driverEmail;
@end