//
// Created by WIINC - Felipe on 05/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface VeiculoAgendarVistoria : JSONModel

@property(nonatomic, strong) NSString *plate;
@property(nonatomic, strong) NSString *renavam;

- (instancetype)initWithPlate:(NSString *)plate renavam:(NSString *)renavam;

+ (instancetype)vistoriaWithPlate:(NSString *)plate renavam:(NSString *)renavam;


@end