//
// Created by WIINC - Felipe on 14/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HistoricoAgendamento;

@interface RegistrarEntrada : JSONModel

@property(nonatomic, strong) HistoricoAgendamento *scheduling;
@property(nonatomic, strong) NSNumber *latitude;
@property(nonatomic, strong) NSNumber *longitude;
@property(nonatomic, strong) NSString *observation;
@property(nonatomic, strong) NSNumber *id;
@property(nonatomic, strong) NSString *entranceDate;


/*
 * "latitude":"-46.3278",
      "longitude":"-46.3278",
      "observation":"oi",
      "id":1,
      "entranceDate":1422008998564
 * */



@end