//
// Created by WIINC - Felipe on 05/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface LojaAgendarVistoria : JSONModel

@property(nonatomic, strong) NSNumber *id;

- (instancetype)initWithId:(NSNumber *)id;

+ (instancetype)vistoriaWithId:(NSNumber *)id;


@end