//
// Created by WIINC - Felipe on 04/10/16.
// Copyright (c) 2016 CSBrasil. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ConsultaVeiculo : JSONModel

@property(nonatomic, strong) NSString *vehiclePlate;
@property(nonatomic, strong) NSString *vehicleRenavam;

@end