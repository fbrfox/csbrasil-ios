//
// Created by pedro henrique borges on 3/5/15.
// Copyright (c) 2015 Iterative. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFHTTPSessionManager.h>


@class ConsultaVeiculoRequest;
@class ConsultaVeiculoResponse;
@class AgendarVistoriaRequest;
@class AgendarVistoriaResponse;
@class ListarAgendamentoRequest;
@class ListarAgendamentoResponse;
@class RegistrarEntradaRequest;


@interface APIClient : AFHTTPSessionManager


+ (APIClient *)sharedManager;

+ (BOOL)autenticado;

- (void)logout;

- (NSURLSessionDataTask *)consultaVeiculo:(ConsultaVeiculoRequest *)request completion:(void (^)(ConsultaVeiculoResponse *response, NSString *errorMessage))completion;


- (NSURLSessionDataTask *)agendarVistoria:(AgendarVistoriaRequest *)request completion:(void (^)(AgendarVistoriaResponse *response, NSString *errorMessage))completion;

- (NSURLSessionDataTask *)solicitarReboque:(AgendarVistoriaRequest *)request completion:(void (^)(AgendarVistoriaResponse *response, NSString *errorMessage))completion;

- (NSURLSessionDataTask *)listarAgendamentos:(ListarAgendamentoRequest *)request completion:(void (^)(ListarAgendamentoResponse *response, NSString *errorMessage))completion;

- (NSURLSessionDataTask *)registrarEntrada:(RegistrarEntradaRequest *)request completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion;

- (NSURLSessionDataTask *)registrarSaida:(AgendarVistoriaRequest *)request completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion;

- (NSURLSessionDataTask *)POSTWithLogParams:(NSString *)URLString
                                 parameters:(NSDictionary *)parameters
                                    success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                    failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)PUTWithLogParams:(NSString *)URLString
parameters:(NSDictionary *)parameters
success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                   failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


- (NSURLSessionDataTask *)GETURLFormatedWithLogParams:(NSString *)URLString
parameters:(NSDictionary *)parameters
success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)GETWithLogParams:(NSString *)URLString
parameters:(NSDictionary *)parameters
success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                   failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;





@end