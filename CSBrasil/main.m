//
//  main.m
//  CSBrasil
//
//  Created by WIINC - Felipe on 08/09/16.
//  Copyright © 2016 CSBrasil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
