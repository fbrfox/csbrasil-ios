//
//  AppDelegate.h
//  CSBrasil
//
//  Created by WIINC - Felipe on 08/09/16.
//  Copyright © 2016 CSBrasil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


- (void)showHome:(UIStoryboard *)storyboard;

- (void)showViewController:(UIViewController *)vc withTransition:(UIViewAnimationOptions)animation;
@end

